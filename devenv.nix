# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).
{ config, pkgs, inputs, ... }:

let
  vscodeExtensions = [
    "jnoortheen.nix-ide"
    "mkhl.direnv"
    "vue.volar"
    "github.copilot"
    "rust-lang.rust-analyzer"
    "golang.go"
    "ms-kubernetes-tools.vscode-kubernetes-tools"
    "vadimcn.vscode-lldb"
  ];
  vscodeExtScriptList = builtins.map
    (i: "${pkgs.vscode}/bin/code --force --install-extension " + i)
    vscodeExtensions;
  vscodeInstallExtScript = builtins.foldl'
    (acc: elem: acc + elem + "\n")
    ""
    vscodeExtScriptList;
in

{

  imports = [
    inputs.comin.nixosModules.comin
    ./sysbox/sysbox.nix
  ];


  networking.hostName = "devenv";

  virtualisation.docker.enable = true;
  virtualisation.sysbox.enable = true;



  # Set your time zone.
  time.timeZone = "America/Los_Angeles";

  # https://www.tweag.io/blog/2020-07-31-nixos-flakes/#pinning-nixpkgs
  nix.registry.nixpkgs.flake = inputs.nixpkgs;

  # Makes it so things that require channels can still work
  # such as nix-shell
  nix.nixPath = [
    "nixpkgs=flake:nixpkgs"
  ];

  system.stateVersion = "23.05"; # Did you read the comment?

  security.sudo.wheelNeedsPassword = false;
  programs.zsh.enable = true;
  users.users.alice = {
    isNormalUser = true;
    extraGroups = [ "wheel" "docker" ]; # Enable ‘sudo’ for the user.
    packages = with pkgs; [ ];
    group = "alice";
    shell = pkgs.zsh;
    # set shell to zsh 
    # passwordFile = "/persist/passwords/alice";
  };
  users.groups.alice = { };
  services.openssh = {
    enable = true;
    # require public key authentication for better security
    settings.PasswordAuthentication = false;
    settings.KbdInteractiveAuthentication = false;
    #settings.PermitRootLogin = "yes";
  };
  users.users."alice".openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFt1Kc7AuNgW0n+Zu4bMfRAWFfScLbzivxNtqC69dTS+ alice@ip-10-0-0-229.us-west-1.compute.internal" # content of authorized_keys file
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDK9oSqErvoipqDl8hx0WEiWeLEfdEOPqbwVzVLNGgRF2s7Nn45DBduZCmpRMSEYDbPOtN+jxa/mj4/omiRv1Y6jTMl1YYzfEOJdwdjhf/T8x1oXbOIsfgoYZnJpUfmIBGaqtSzIU/zWUKYENc6EtfzEjV98tdtxPd23QpzNWsXTD2BcIYEizGD75lbnDBb2EZQZhnNnTk62zzxL42pQ7g6SdVGBmVDV4IduUGPX2O1AF9DeIeorrrTkqeTSVmK8Q86GN33Tx7ClQIqWLzwanMbCYeFvpl8/Y3AMDVFGPwcAm/6MpiKR6XkxuSL5zYQ6Ys3c9L+drd1bnpmWRSsfm0OdeUj9DaCVWY7oQ5O/KviwBVu+J5AoNQDxfcX+a+KKDqHO0q6N0Gd5xMaB0rRILkJRRteg65tSaLLWVQKwjcdU9ydbmC6GC2hCxgVtipTIgispp+GqOk+5c/NZEZ+zkUuNhsmbRNsgANxbgrbNrfATTk1T7fMiQZFFtfuzhjg86M= robertwendt@roberts-air.lan"

    # note: ssh-copy-id will add user@your-machine after the public key
    # but we can remove the "@your-machine" part
  ];

  environment.systemPackages = with pkgs; [
    mold
    mold-wrapped
    wget
    git
    openssh
    pkgs.thefuck
    nixpkgs-fmt
    stow
    lsof
    openssh
    htop
    nodejs
    go
    rustup
    cntr
    vscode
    (pkgs.writeShellScriptBin "restart-vscode-node-procs" ''
      ps aux | \
      grep ${pkgs.nodejs} | \
      awk '{print $2, $11 }' | \
      grep ${pkgs.nodejs} | \
      awk '{print $1}' | xargs kill -9
    '')
    comma
    k3s
    kubernetes-helm
    kubectl
    jq
    awscli2
    ripgrep
    atuin
  ];
  # set default editor to vim 
  environment.variables = { EDITOR = "nvim"; };
  programs.neovim.enable = true;
  programs.neovim.defaultEditor = true;
  programs.direnv.enable = true;
  nixpkgs.config.allowUnfree = true;
  services.tailscale.enable = true;


  nix = {
    settings = {
      substituters = [
        "https://cache.garnix.io"
        "https://nix-community.cachix.org"
        "https://cache.nixos.org/"
      ];
      trusted-public-keys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
        "cache.garnix.io:CTFPyKSLcx5RMJKfLo5EEPUObbA78b0YQ2DTCJXqr9g="
      ];
    };
  };


  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  # enable tailscale up on boot
  # auth key can be found in /persist/ts/authkey

  # create a oneshot job to authenticate to Tailscale
  systemd.services.tailscale-autoconnect = {
    description = "Automatic connection to Tailscale";

    # make su`re tailscale is running before trying to connect to tailscale
    after = [ "network-pre.target" "tailscale.service" ];
    wants = [ "network-pre.target" "tailscale.service" ];
    wantedBy = [ "multi-user.target" ];

    # set this service as a oneshot job
    serviceConfig.Type = "oneshot";

    # have the job run this shell script
    script = with pkgs; ''
      # wait for tailscaled to settle
      sleep 2

      # check if we are already authenticated to tailscale
      status="$(${tailscale}/bin/tailscale status -json | ${jq}/bin/jq -r .BackendState)"
      if [ $status = "Running" ]; then # if so, then do nothing
        exit 0
      fi

      # otherwise authenticate with tailscale
      ${tailscale}/bin/tailscale up --ssh -authkey `cat /tsauthkey` --hostname devenv
    '';
  };

  systemd.services.vscode-install-extensions = {
    description = "vscode install extensions";
    after = [ "network-pre.target" ];
    wants = [ "network-pre.target" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig.Type = "simple";
    serviceConfig.User = "alice";
    # script = ''
    #   ${pkgs.vscode}/bin/code --force --install-extension jnoortheen.nix-ide 
    # '';
    script = vscodeInstallExtScript;
    serviceConfig.WorkingDirectory = "/home/alice";
    serviceConfig.Environment = "PATH=${pkgs.git}/bin:${pkgs.nix}/bin:/run/current-system/sw/bin:/usr/bin:/bin";
  };

  systemd.services.vscode-server = {
    description = "vscode serve-web";
    after = [ "network-pre.target" "tailscale.service" ];
    wants = [ "network-pre.target" "tailscale.service" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig.Type = "simple";
    serviceConfig.User = "alice";
    script = ''
      ${pkgs.vscode}/bin/code serve-web --without-connection-token --host 0.0.0.0 --port 4321 --extensions-dir /home/alice/.vscode/extensions | ${pkgs.nix}/bin/nix run github:r33drichards/fix-vscode-server ${pkgs.nodejs}/bin/node
      # useful for debugging
      # ${pkgs.nix}/bin/nix run /home/alice/fix-vscode-server ${pkgs.nodejs}/bin/node

    '';
    serviceConfig.WorkingDirectory = "/home/alice";
    serviceConfig.Environment = "PATH=${pkgs.git}/bin:${pkgs.nix}/bin:/run/current-system/sw/bin:/usr/bin:/bin";

  };

  # tailscale serve port 3210 after vscode server comes up

  systemd.services.tailscale-serve = {
    description = "Tailscale Serve";
    after = [ "network-pre.target" "tailscale.service" ];
    wants = [ "network-pre.target" "tailscale.service" ];
    requires = [ "vscode-server.service" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig.Type = "simple";
    # https://www.redhat.com/sysadmin/systemd-automate-recovery
    serviceConfig.Restart = "on-failure";
    # have the job run this shell script
    script = with pkgs; ''
      sleep 2
      ${tailscale}/bin/tailscale serve 4321;
    '';
  };


  # chown /home/alice/.ssh to alice
  # set permissions for /home/alice/.ssh/id_ed25519 to 600
  systemd.services.fix-ssh-permissions = {
    description = "Fix SSH permissions";
    after = [ "network-pre.target" "tailscale.service" ];
    wants = [ "network-pre.target" "tailscale.service" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig.Type = "oneshot";
    serviceConfig.Environment = "PATH=${pkgs.openssh}/bin:/run/current-system/sw/bin:/usr/bin:/bin";

    script = ''
      chmod 400 /home/alice/.ssh/id_rsa
      ssh-keyscan -H gitlab.com >> /home/alice/.ssh/known_hosts
      ssh-keyscan -H github.com >> /home/alice/.ssh/known_hosts
      chown -R alice /home/alice/.ssh
      chown alice /GITLAB_ACCESS_TOKEN
      chown alice /dash-env
    '';
  };

  # after ssh permissions are fixed, clone some git repos
  systemd.services.clone-git-repos = {
    description = "Clone git repos";
    after = [ "fix-ssh-permissions.service" ];
    wants = [ "fix-ssh-permissions.service" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig.Type = "simple";
    serviceConfig.Environment = "PATH=${pkgs.git}/bin:${pkgs.nix}/bin:/run/current-system/sw/bin:/usr/bin:/bin";
    serviceConfig.User = "alice";

    script = ''
      cd /home/alice
      # if the directory doesn't exist, clone the repo
      if [ ! -d /home/alice/aws-nixos ]; then
        git clone git@gitlab.com:reedrichards/aws-nixos.git
      fi
      # git@gitlab.com:reedrichards/reedrichards.gitlab.io.git
      if [ ! -d /home/alice/reedrichards.gitlab.io ]; then
        git clone git@gitlab.com:reedrichards/reedrichards.gitlab.io.git
      fi

      # git@gitlab.com:reedrichards/resume.git
      if [ ! -d /home/alice/resume ]; then
        git clone git@gitlab.com:reedrichards/resume.git
      fi

      # git@gitlab.com:reedrichards/tfe.git
      if [ ! -d /home/alice/tfe ]; then
        git clone git@gitlab.com:reedrichards/tfe.git
      fi

      # git@gitlab.com:reedrichards/tigris.git
      if [ ! -d /home/alice/tigris ]; then
        git clone git@gitlab.com:reedrichards/tigris.git
      fi

      # git@gitlab.com:reedrichards/pbin.git
      if [ ! -d /home/alice/pbin ]; then
        git clone git@gitlab.com:reedrichards/pbin.git
      fi

      # git@gitlab.com:reedrichards/wvdom.git
      if [ ! -d /home/alice/wvdom ]; then
        git clone git@gitlab.com:reedrichards/wvdom.git
      fi

      # git@gitlab.com:reedrichards/othello.git
      if [ ! -d /home/alice/othello ]; then
        git clone git@gitlab.com:reedrichards/othello.git
      fi  
      

      # git@github.com:reedrichards/othello.git
      if [ ! -d /home/alice/othello ]; then
        git clone git@gitlab.com:reedrichards/othello.git
      fi  

      # git@github.com:getflakery/flakes.git
      if [ ! -d /home/alice/flakes ]; then
        git clone git@github.com:getflakery/flakes.git
      fi  

      # git@github.com:getflakery/dash.git
      if [ ! -d /home/alice/dash ]; then
        git clone git@github.com:getflakery/dash.git
      fi  


      ${pkgs.nix}/bin/nix run github:r33drichards/list-starred-repos 


    '';
  };

  systemd.services.symlink-env-file = {
    # create a symlink that allows two way edits 
    # from /dash-env to /home/alice/dash/.env
    # make this service dependent on clone-git-repos
    description = "Symlink .env file";
    after = [ "clone-git-repos.service" ];
    wants = [ "clone-git-repos.service" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig.Type = "oneshot";
    serviceConfig.User = "alice";
    script = ''
      ln -s  /home/alice/dash/.env /dash-env
    '';

  };

  # find /workspace -type f -name .envrc -print0 | xargs -0 -t -I % -P $(nproc) direnv allow % && exit
  systemd.services.direnv-allow = {
    description = "direnv allow";
    after = [ "clone-git-repos.service" ];
    wants = [ "clone-git-repos.service" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig.Type = "oneshot";
    serviceConfig.User = "alice";

    script = ''
      find /home/alice -type f -name .envrc -print0 | xargs -0 -t -I % -P $(nproc) ${pkgs.direnv}/bin/direnv allow % && exit
    '';
  };

  systemd.services.add-channel-for-rust-analyzer = {
    description = "Add rust-analyzer channel";
    after = [ "network-pre.target" ];
    wants = [ "network-pre.target" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig.Type = "oneshot";
    script = ''
      nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
      nix-channel --update
    '';
  };



  services.redis.vmOverCommit = true;
  services.redis.servers."tigris".enable = true;
  services.redis.servers."tigris".port = 6379;


  services.comin = {
    enable = true;
    hostname = "hello";
    remotes = [
      {
        name = "origin";
        url = "https://gitlab.com/reedrichards/aws-nixos";
        poller.period = 2;
      }
    ];
  };

  boot.kernel.sysctl = {
    "fs.inotify.max_user_watches" = 524288;
  };



  # enable k3s
  networking.firewall.allowedTCPPorts = [
    6443 # k3s: required so that pods can reach the API server (running on port 6443 by default)
    # 2379 # k3s, etcd clients: required if using a "High Availability Embedded etcd" configuration
    # 2380 # k3s, etcd peers: required if using a "High Availability Embedded etcd" configuration
  ];
  networking.firewall.allowedUDPPorts = [
    # 8472 # k3s, flannel: required if using multi-node for inter-node networking
  ];
  services.k3s.enable = true;
  services.k3s.role = "server";
  services.k3s.extraFlags = toString [
    # "--kubelet-arg=v=4" # Optionally add additional args to k3s
    "--write-kubeconfig-mode 644"
  ];

  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 7d";
  };
  virtualisation.containers.enable = true;
  virtualisation = {
    podman = {
      enable = true;

      # Create a `docker` alias for podman, to use it as a drop-in replacement
      dockerCompat = true;
      # docker socket
      dockerSocket.enable = true;

      # Required for containers under podman-compose to be able to talk to each other.
      defaultNetwork.settings.dns_enabled = true;
    };
  };
  networking.firewall.interfaces.podman0.allowedUDPPorts = [ 53 ];

  # make nocodb depend on create-dir
  virtualisation.oci-containers.backend = "podman";


}
