#! /usr/bin/env nix-shell
#! nix-shell --pure -i bash -p patchelf nix


# interpreter=$(nix eval --raw '((import <nixpkgs> { }).stdenv.glibc)')/lib/ld-linux-x86-64.so.2
# libstdcxx=$(nix eval --raw '((import <nixpkgs> { }).stdenv.cc.cc.lib)')/lib/libstdc++.so.6
# error: '((import <nixpkgs> { }).stdenv.glibc)' is not a valid URL
# error: '((import <nixpkgs> { }).stdenv.cc.cc.lib)' is not a valid URL


# interpreter=$(nix eval --raw -f '<nixpkgs>' 'stdenv.cc.bintools.dynamicLinker')
# libstdcxx=$(nix eval --raw -f '<nixpkgs>' 'stdenv.cc.cc.lib')

interpreter='/home/alice/aws-nixos/ld-linux-x86-64.so.2'
libstdcxx='/home/alice/aws-nixos/pp0jsd045xvfsz60kpbkfxbs9pbpk8z5-gcc-13.2.0-lib'
echo $interpreter
echo $libstdcxx
for dir in ~/.vscode-server/bin/*; do
  cd $dir

  patchelf \
    --set-interpreter $interpreter \
    --replace-needed libstdc++.so.6 $libstdcxx \
    node

  patchelf \
    --replace-needed libstdc++.so.6 $libstdcxx \
    node_modules/node-pty/build/Release/pty.node
done