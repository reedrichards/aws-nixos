# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ config, pkgs, ... }:

{


  networking.hostName = "pbin";


  # Set your time zone.
  time.timeZone = "America/Los_Angeles";

  # users.users.root.passwordFile = "/persist/passwords/root";
  # # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.alice = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    packages = with pkgs; [ ];
    # set shell to zsh 
    # passwordFile = "/persist/passwords/alice";
  };

  # allow alice to run sudo without password
  security.sudo.wheelNeedsPassword = false;


  system.stateVersion = "23.05"; # Did you read the comment?
  nixpkgs.config.allowUnfree = true;

  services.tailscale = {
    enable = true;
    authKeyFile = "/tsauthkey";
    extraUpFlags = ["--ssh" "--hostname" "pbin"];
  };

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  systemd.services.tailscale-funnel = {
    description = "Tailscale funnel";
    after = [ "network-pre.target" "tailscale.service" ];
    wants = [ "network-pre.target" "tailscale.service" ];
    requires = [ "pbin.service" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig.Type = "simple";
    # https://www.redhat.com/sysadmin/systemd-automate-recovery
    serviceConfig.Restart = "on-failure";
    # have the job run this shell script
    script = with pkgs; ''
      sleep 2
      ${tailscale}/bin/tailscale funnel 8000;
    '';
  };



  systemd.services.pbin = {
    description = "pbin";
    # Service configuration
    serviceConfig = {
      Type = "simple";
      ExecStart = "${pkgs.nix}/bin/nix run gitlab:reedrichards/pbin --refresh";
      User = "alice";
      EnvironmentFile = "/persist/pbin.env";
      Environment = "PATH=${pkgs.git}/bin:${pkgs.nix}/bin:/run/current-system/sw/bin:/usr/bin:/bin";
    };

    wantedBy = [ "default.target" ];
  };

}
