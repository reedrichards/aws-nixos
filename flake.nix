{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs?ref=nixos-23.11";
    home-manager.url = "github:nix-community/home-manager?ref=release-23.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    fix-vscode-server.url = "github:r33drichards/fix-vscode-server";
    comin.url = "github:r33drichards/comin/8f8352537ca4ecdcad06b1b4ede4465d37dbd00c";
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    vscode-server.url = "github:nix-community/nixos-vscode-server";

  };
  outputs =
    inputs@{ nixpkgs
    , home-manager
    , fix-vscode-server
    , comin
    , nixos-generators
    , vscode-server
    , ...
    }: {
      # replace 'joes-desktop' with your hostname here.
      nixosConfigurations.nixos = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = {
          inherit inputs;
        };
        modules = [
          ./configuration.nix
          ./nixos.nix
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.alice = {
              home.stateVersion = "23.05"; # Please read the comment before changing.

              programs.git = {
                enable = true;
                userName = "rw";
                userEmail = "rw@jjk.is";
              };
              programs.zsh = {
                enable = true;
                enableCompletion = false; # enabled in oh-my-zsh
                # initExtra = ''
                #   source ${nixpkgs.zsh-powerlevel10k}/share/zsh-powerlevel10k/powerlevel10k.zsh-theme
                # '' + builtins.readFile ./p10k.zsh;
                shellAliases = {
                  switch = "sudo nixos-rebuild  switch --flake '/home/alice/aws-nixos#hello' --refresh";

                };
                oh-my-zsh = {
                  enable = true;
                  plugins = [ "git" "thefuck" "direnv" ];
                  theme = "robbyrussell";
                };
              };
            };
          }
        ];
      };
      nixosConfigurations.hello = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = {
          inherit inputs;
        };
        modules = [
          ./configuration.nix
          ./devenv.nix
          # # pass home-manager to the hello configuration
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.alice = {
              home.stateVersion = "23.05"; # Please read the comment before changing.

              programs.atuin = {
                enable = true;
                settings = {
                  # Uncomment this to use your instance
                  # sync_address = "https://majiy00-shell.fly.dev";
                };
              };

              programs.git = {
                enable = true;
                userName = "r33drichards";
                userEmail = "rwendt1337@gmail.com";
              };
              programs.zsh = {
                enable = true;
                enableCompletion = false; # enabled in oh-my-zsh
                # initExtra = ''
                #   source ${nixpkgs.zsh-powerlevel10k}/share/zsh-powerlevel10k/powerlevel10k.zsh-theme
                # '' + builtins.readFile ./p10k.zsh;
                shellAliases = {
                  switch = "sudo nixos-rebuild  switch --flake '/home/alice/aws-nixos#hello' --refresh";


                };
                oh-my-zsh = {
                  enable = true;
                  plugins = [ "git" "thefuck" "direnv" ];
                  theme = "robbyrussell";
                };
              };
            };
          }
          vscode-server.nixosModules.default
          ({ config, pkgs, ... }: {
            services.vscode-server.enable = true;
          })
        ];
      };
      nixosConfigurations.pbin = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = {
          inherit inputs;
        };
        modules = [
          ./configuration.nix
          ./pbin.nix
        ];
      };
      nixosConfigurations.bcache = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = {
          inherit inputs;
        };
        modules = [
          ./configuration.nix
          ./bcache.nix
        ];
      };

      packages.x86_64-linux = {
        tailscale = nixos-generators.nixosGenerate {
          system = "x86_64-linux";
          format = "amazon";
          modules = [
            ./configuration.nix
            ./tailscale.nix
          ];
        };
      };
    };
}
