# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ config, pkgs, ... }:

{

  networking.hostName = "bcache";


  # Set your time zone.
  time.timeZone = "America/Los_Angeles";

  # users.users.root.passwordFile = "/persist/passwords/root";
  # # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.alice = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    packages = with pkgs; [ ];
    # set shell to zsh 
    # passwordFile = "/persist/passwords/alice";
  };

  # allow alice to run sudo without password
  security.sudo.wheelNeedsPassword = false;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    git
    openssh
    lsof
    openssh
    htop
  ];
  programs.direnv.enable = true;
  # set default editor to vim 
  environment.variables = { EDITOR = "nvim"; };
  programs.neovim.enable = true;
  programs.neovim.defaultEditor = true;


  system.stateVersion = "23.05"; # Did you read the comment?
  nixpkgs.config.allowUnfree = true;
  services.tailscale.enable = true;


  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  nix.settings.substituters = [
    "https://cache.nixos.org"
    "https://bcache-hake.ts.net"
    "https://nix-community.cachix.org"
  ];
  nix.settings.trusted-public-keys = [
    "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
    "bcache-hake.ts.net:GmWXVjrkMZQUkIrPpAPTwQLkzGoOZnUe7/9Vsyu2dl4="
    "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="

  ];

  # enable tailscale up on boot
  # auth key can be found in /persist/ts/authkey

  # create a oneshot job to authenticate to Tailscale
  systemd.services.tailscale-autoconnect = {
    description = "Automatic connection to Tailscale";

    # make sure tailscale is running before trying to connect to tailscale
    after = [ "network-pre.target" "tailscale.service" ];
    wants = [ "network-pre.target" "tailscale.service" ];
    wantedBy = [ "multi-user.target" ];

    # set this service as a oneshot job
    serviceConfig.Type = "oneshot";

    # have the job run this shell script
    script = with pkgs; ''
      # wait for tailscaled to settle
      sleep 2

      # check if we are already authenticated to tailscale
      status="$(${tailscale}/bin/tailscale status -json | ${jq}/bin/jq -r .BackendState)"
      if [ $status = "Running" ]; then # if so, then do nothing
        exit 0
      fi

      # otherwise authenticate with tailscale
      ${tailscale}/bin/tailscale up --ssh -authkey `cat /tsauthkey` --hostname bcache
    '';
  };

  networking.firewall = {
    enable = true;
    allowedTCPPorts = [ 80 443 ];
    # allowedUDPPortRanges = [
    #   { from = 4000; to = 4007; }
    #   { from = 8000; to = 8010; }
    # ];
  };


  services.nix-serve = {
    enable = true;
    secretKeyFile = "/persist/cache-priv-key.pem";
  };

  # systemd services to chown "/persist/cache-priv-key.pem" to nix-serve and chmod 600 cache-priv-key.pem

  systemd.services.chown-key = {
    description = "chown /persist/cache-priv-key.pem";
    serviceConfig.Type = "oneshot";
    script = ''
      chown nix-serve /persist/cache-priv-key.pem
      chmod 600 /persist/cache-priv-key.pem
    '';
  };

  systemd.services.tailscale-funnel = {
    description = "Tailscale funnel";
    after = [ "network-pre.target" "tailscale.service" ];
    wants = [ "network-pre.target" "tailscale.service" ];
    requires = [ "pbin.service" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig.Type = "simple";
    # https://www.redhat.com/sysadmin/systemd-automate-recovery
    serviceConfig.Restart = "on-failure";
    # have the job run this shell script
    script = with pkgs; ''
      sleep 2
      ${tailscale}/bin/tailscale funnel ${toString config.services.nix-serve.port};
    '';
  };






}
