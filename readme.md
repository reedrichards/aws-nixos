
# clone this repo 

```
git clone https://gitlab.com/reedrichards/aws-nixos
```

# helpful commands 
```bash



echo 'tskey-auth-secret-secret' >/tsauthkey
sudo nixos-rebuild  switch --flake 'gitlab:reedrichards/aws-nixos#hello' --refresh

sudo nixos-rebuild  switch --flake 'github:r33drichards/basic-flake#hello-flakery' --refresh


sudo nixos-rebuild  switch --flake 'gitlab:reedrichards/aws-nixos#nixos' --refresh
sudo nixos-rebuild  switch --flake 'gitlab:reedrichards/aws-nixos#pbin' --refresh

sudo nixos-rebuild  switch --flake 'gitlab:reedrichards/aws-nixos#bcache' --refresh


sudo systemctl status deleteec2.service

sudo nixos-rebuild  switch --flake '.#hello' --refresh --show-trace
sudo nixos-rebuild  switch --flake '/home/alice/aws-nixos#nixos' --refresh
sudo nixos-rebuild  switch --flake '/home/alice/aws-nixos#bcache' --refresh
sudo nixos-rebuild  switch --flake '/home/alice/aws-nixos#hello' --refresh

sudo nixos-rebuild  switch --flake '.#hello' --refresh


sudo nixos-rebuild  switch --show-trace --flake '.#avsc' --refresh

# apply the 'gitlab:reedrichards/aws-nixos#nixos' flake to alice@prod (ssh hostname is prod)
# don't forget to git push
nixos-rebuild --target-host alice@prod --use-remote-sudo switch --flake 'gitlab:reedrichards/aws-nixos#nixos' 

nixos-rebuild --build-host alice@prod  --target-host alice@prod --use-remote-sudo switch --flake 'gitlab:reedrichards/aws-nixos#nixos' --refresh

nixos-rebuild --target-host alice@dev --use-remote-sudo switch --flake '.#hello' --refresh


ssh alice@prod 'sudo systemctl status deleteec2.service'
ssh alice@prod 'sudo systemctl status deleteec2.timer'

ssh alice@prod 'sudo systemctl stop deleteec2.service'
ssh alice@prod 'sudo systemctl stop deleteec2.timer'
ssh alice@prod 'sudo systemctl start deleteec2.timer'

ssh alice@prod 'sudo journalctl -xeu deleteec2.service -f'
ssh alice@prod 'nix run github:r33drichards/delete_ec2 --refresh'
```

# bootstrapping prod 

## [create](https://us-west-1.console.aws.amazon.com/ec2/home?region=us-west-1#LaunchTemplateDetails:launchTemplateId=lt-0d7b76529ceabcb50) an ec2 instance 

## apply configuration
```
sudo nixos-rebuild  switch --flake 'gitlab:reedrichards/aws-nixos#nixos' --refresh
```

## set tsauthkey

```
sudo su 
echo 'thekey' > /tsauthkey
```

## configure aws 

```
nix-shell -p awscli2 --run 'aws configure'
```

## create and populate `/persist/pbin.env`, `/persist/tigris.env`, and `/persist/tigris-staging.env`


```
sudo mkdir /persist
sudo chown -R alice /persist
cat > /persist/tigris.env <<EOF
PORT=8001
KEY_PATH=/persist/tbin.pem
DB_PATH=/persist/tigris.db
AUTH_TOKEN='RANDOMSTRING'
EOF

cat > /persist/tigris-staging.env <<EOF
PORT=8002
KEY_PATH=/persist/tbin.pem
DB_PATH=/persist/tigris-staging.db
AUTH_TOKEN='RANDOMSTRING'
EOF

```

## binary cache for flakery adapted from https://nixos.wiki/wiki/Binary_Cache

```
nix-store --generate-binary-cache-key bcache-hake.ts.net cache-priv-key.pem cache-pub-key.pem

```

# flakery base image 

```
nix build .#tailscale --impure
aws s3 cp result/nixos-amazon-image-23.11.20240316.8ac30a3-x86_64-linux.vhd s3://nixos-base/nixos-amazon-image-23.11.20240316.8ac30a3-x86_64-linux.vhd
aws ec2 import-snapshot --description "flakery nixos" --disk-container "file://flakery-base/containers.json"   
```

```
{
    "Description": "flakery nixos",
    "ImportTaskId": "import-snap-01c750a9b69d61f1e",
    "SnapshotTaskDetail": {
        "Description": "flakery nixos",
        "DiskImageSize": 0.0,
        "Progress": "0",
        "Status": "active",
        "StatusMessage": "pending",
        "Url": "s3://nixos-base/nixos-amazon-image-23.11.20240316.8ac30a3-x86_64-linux.vhd"
    },
    "Tags": []
}
```


```
watch "aws ec2 describe-import-snapshot-tasks --import-task-ids import-snap-01c750a9b69d61f1e"  
```

```

{
    "ImportSnapshotTasks": [
        {
            "Description": "flakery nixos",
            "ImportTaskId": "import-snap-01c750a9b69d61f1e",
            "SnapshotTaskDetail": {
                "Description": "flakery nixos",
                "DiskImageSize": 1688628224.0,
                "Format": "VHD",
                "SnapshotId": "snap-0523cd0d0571f5e48",
                "Status": "completed",
                "Url": "s3://nixos-base/nixos-amazon-image-23.11.20240316.8ac30a3-x86_64-linux.vhd
",
                "UserBucket": {
                    "S3Bucket": "nixos-base",
                    "S3Key": "nixos-amazon-image-23.11.20240316.8ac30a3-x86_64-linux.vhd"
                }
            },
            "Tags": []
        }
    ]
}
```

```
[root@ip-10-0-0-184:~]# curl http://169.254.169.254/latest/meta-data/tags/instance
Name
foo
[root@ip-10-0-0-184:~]# curl http://169.254.169.254/latest/meta-data/tags/instance/foo
bar
```