{ config
, lib
, pkgs
, ...
}:

with lib;

let
  cfg = config.virtualisation.sysbox;
  sysbox = pkgs.stdenv.mkDerivation rec {
    pname = "sysbox";
    version = "0.6.2";

    src = pkgs.fetchurl {
      url = "https://downloads.nestybox.com/sysbox/releases/v0.6.2/sysbox-ce_${version}-0.linux_amd64.deb";
      sha256 = "/Sh/LztaBytiw3j54e7uqizK0iu0jLOB0w2MhVxRtAE=";
    };

    nativeBuildInputs = [ pkgs.dpkg ];

    unpackPhase = ''
      dpkg-deb -R $src .
    '';

    installPhase = ''
      mkdir -p $out/bin
      cp -rf usr/bin/* $out/bin/
    '';

    meta = {
      homepage = "https://github.com/nestybox/sysbox";
      description = "An open-source, next-generation 'runc' that empowers rootless containers to run workloads such as Systemd, Docker, Kubernetes, just like VMs.";
      license = licenses.asl20;
      maintainers = with maintainers; [ ];
    };
  };
in

{
  ###### interface

  options.virtualisation.sysbox = {
    enable =
      mkOption {
        type = types.bool;
        default = false;
        description =
          lib.mdDoc ''
            This option enables sysbox
          '';
      };
    package = mkOption {
      type = types.package;
      default = sysbox;
      description = "The Sysbox package.";
    };

    # package = sysbox;
  };

  ###### implementation

  config = mkIf cfg.enable {
    systemd.services.sysbox-mgr = {
      description = "Sysbox Manager Service";
      wantedBy = [ "multi-user.target" ];

      path = [ pkgs.rsync pkgs.kmod pkgs.iptables ];
      script = "${cfg.package}/bin/sysbox-mgr";

      preStart = ''
        mkdir /sbin || true
        cp ${pkgs.iptables}/bin/* /sbin || true
      '';

      serviceConfig = {
        User = "root";
        Group = "root";
      };
    };

    systemd.services.sysbox-fs = {
      description = "Sysbox FileSystem Service";
      wantedBy = [ "multi-user.target" ];

      path = [ pkgs.rsync pkgs.kmod pkgs.fuse pkgs.iptables ];
      script = "${cfg.package}/bin/sysbox-fs";

      serviceConfig = {
        User = "root";
        Group = "root";
      };
    };

    virtualisation.docker.extraOptions = ''--add-runtime=sysbox=${cfg.package}/bin/sysbox-runc'';

    security.unprivilegedUsernsClone = true;

    # assertions = [
    #   {
    #     assertion = !virtualisation.docker.enable;
    #     message = "Sysbox require docker to be functional";
    #   }
    #   {
    #     assertion = virtualisation.podman.enable;
    #     message = "Sysbox require docker to be functional";
    #   }
    # ];
  };
}
