# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ config, pkgs, inputs, ... }:

{

  imports = [
    inputs.comin.nixosModules.comin
  ];

  networking.hostName = "prod";


  # Set your time zone.
  time.timeZone = "America/Los_Angeles";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";


  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e,caps:escape";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Persisting user passwords 
  # to create the password files, run:
  # $ sudo su
  # $ nix-shell -p mkpasswd
  # $ mkdir -p /persist/passwords
  # $ mkpasswd -m sha-512 > /persist/passwords/root
  # $ mkdir -p /persist/passwords
  # $ mkpasswd -m sha-512 > /persist/passwords/alice
  # users.mutableUsers = false;
  programs.zsh.enable = true;
  # users.users.root.passwordFile = "/persist/passwords/root";
  # # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.alice = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    packages = with pkgs; [ ];
    # set shell to zsh 
    shell = pkgs.zsh;
    # passwordFile = "/persist/passwords/alice";
  };

  # allow alice to run sudo without password
  security.sudo.wheelNeedsPassword = false;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    git
    openssh
    pkgs.thefuck
    nixpkgs-fmt
    stow
    tmux
    lsof
    openssh
    htop
    redis
  ];
  programs.direnv.enable = true;
  # set default editor to vim 
  environment.variables = { EDITOR = "nvim"; };
  programs.neovim.enable = true;
  programs.neovim.defaultEditor = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
  nixpkgs.config.allowUnfree = true;
  services.tailscale.enable = true;


  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # enable tailscale up on boot
  # auth key can be found in /persist/ts/authkey

  # create a oneshot job to authenticate to Tailscale
  systemd.services.tailscale-autoconnect = {
    description = "Automatic connection to Tailscale";

    # make sure tailscale is running before trying to connect to tailscale
    after = [ "network-pre.target" "tailscale.service" ];
    wants = [ "network-pre.target" "tailscale.service" ];
    wantedBy = [ "multi-user.target" ];

    # set this service as a oneshot job
    serviceConfig.Type = "oneshot";

    # have the job run this shell script
    script = with pkgs; ''
      # wait for tailscaled to settle
      sleep 2

      # check if we are already authenticated to tailscale
      status="$(${tailscale}/bin/tailscale status -json | ${jq}/bin/jq -r .BackendState)"
      if [ $status = "Running" ]; then # if so, then do nothing
        exit 0
      fi

      # otherwise authenticate with tailscale
      ${tailscale}/bin/tailscale up --ssh -authkey `cat /tsauthkey`
    '';
  };

  networking.firewall = {
    enable = true;
    allowedTCPPorts = [ 80 443 ];
    # allowedUDPPortRanges = [
    #   { from = 4000; to = 4007; }
    #   { from = 8000; to = 8010; }
    # ];
  };

  services.caddy = {
    enable = true;
    virtualHosts = {
      "staging.nixos.jjk.is".extraConfig = ''
        handle /* {
          reverse_proxy http://127.0.0.1:8002
        }

      '';
      "nixos.jjk.is".extraConfig = ''
        handle_path /irc/* {
            reverse_proxy http://127.0.0.1:9000
        }
        handle /* {
          reverse_proxy http://127.0.0.1:8001
        }

      '';
      "pbin.jjk.is".extraConfig = ''
        redir https://p.jjk.is{uri}
      '';
      "p.jjk.is".extraConfig = ''
        reverse_proxy  http://127.0.0.1:8000
      '';
    };
  };


  # weechat sytemd service launced as alice user, in tmux

  # Go read the terms at https://letsencrypt.org/repository/


  systemd.services.tigris = {
    description = "tigris";
    # Service configuration
    serviceConfig = {
      Type = "simple";
      # ExecStart = "${pkgs.nix}/bin/nix run /home/alice/nercel --refresh";
      ExecStart = "${pkgs.nix}/bin/nix run gitlab:reedrichards/tigris?ref=main --refresh";
      User = "alice";
      EnvironmentFile = "/persist/tigris.env";
      Environment = "PATH=${pkgs.git}/bin:${pkgs.nix}/bin:/run/current-system/sw/bin:/usr/bin:/bin";
    };

    wantedBy = [ "default.target" ];
  };

  systemd.services.tigris-staging = {
    description = "tigris-staging";
    # Service configuration
    serviceConfig = {
      Type = "simple";
      # ExecStart = "${pkgs.nix}/bin/nix run /home/alice/nercel --refresh";
      ExecStart = "${pkgs.nix}/bin/nix run gitlab:reedrichards/tigris?ref=staging --refresh";

      User = "alice";
      EnvironmentFile = "/persist/tigris-staging.env";
      Environment = "PATH=${pkgs.git}/bin:${pkgs.nix}/bin:/run/current-system/sw/bin:/usr/bin:/bin";
    };

    wantedBy = [ "default.target" ];
  };


  services.redis.servers."tigris".enable = true;
  services.redis.servers."tigris".port = 6379;
  services.redis.servers."tigris-staging".enable = true;
  services.redis.servers."tigris-staging".port = 6380;



  systemd.services.deleteec2 = {
    description = "delete";
    # Service configuration
    serviceConfig = {
      Type = "oneshot";
      ExecStart = "${pkgs.nix}/bin/nix run github:r33drichards/delete_ec2 --refresh";
      User = "alice";
      Environment = "PATH=${pkgs.git}/bin:${pkgs.nix}/bin:/run/current-system/sw/bin:/usr/bin:/bin";
    };

    wantedBy = [ "default.target" ];
  };

  systemd.timers.deleteec2 = {
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnBootSec = "20m";
      OnUnitActiveSec = "20m";
      Unit = "deleteec2.service";
    };
  };

  systemd.services.instance-queue = {
    description = "instance-queue";
    path = [ pkgs.git pkgs.nix ];
    # Service configuration
    serviceConfig = {
      Type = "simple";
      ExecStart = "${pkgs.nix}/bin/nix run github:r33drichards/iq#default --refresh";
      User = "alice";
      EnvironmentFile = "/persist/iq.env";
    };

    wantedBy = [ "default.target" ];
  };

  services.comin = {
    enable = true;
    hostname = "nixos";
    remotes = [
      {
        name = "origin";
        url = "https://gitlab.com/reedrichards/aws-nixos";
        poller.period = 2;
      }
    ];
  };


nix.distributedBuilds = true;



}
